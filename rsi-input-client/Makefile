SHELL := /bin/bash

BUILD_ROOT ?= ${shell pwd}

VENDOR_DIR=$(BUILD_ROOT)

ifeq ($(ARCH),arm64)
TARGET_DIR?=$(BUILD_ROOT)/target.$(ARCH)
else
TARGET_DIR?=$(BUILD_ROOT)/target
endif

TARGET_INC_DIR = $(TARGET_DIR)/usr/local/include
TARGET_LIB_DIR = $(TARGET_DIR)/usr/local/lib

ifeq ($(ARCH),arm64)
CONFIG_ARGS:=-DCMAKE_PREFIX_PATH=$(TARGET_DIR)/usr/local -DCMAKE_SYSROOT=$(SDKTARGETSYSROOT) -DCMAKE_SYSTEM_LIBRARY_PATH=$(SDKTARGETSYSROOT)/usr/lib64
else
CONFIG_ARGS:=-DCMAKE_PREFIX_PATH=$(TARGET_DIR)/usr/local
librsi_dev_path:=$(BUILD_ROOT)/sdk/x86/librsi-dev-linux-x86.tar.gz
poco_ver=1.9.0
libpoco_src_path:=$(BUILD_ROOT)/sdk/x86/poco-$(poco_ver).tar.gz
endif

ifeq ($(ARCH),arm64)
BUILD_DIR=build.$(ARCH)
else
BUILD_DIR=build$(ARCH)
endif

.PHONY : build install sdk poco

all: configure build

build:
	@echo "[$@]"
	cd $(VENDOR_DIR)/$(BUILD_DIR) ;	\
	$(MAKE) VERBOSE=1 ;	\
	$(MAKE) install DESTDIR=$(TARGET_DIR)

clean:
	@echo "[$@]"
	@ $(RM) -rf $(VENDOR_DIR)/$(BUILD_DIR)
	@echo "[$@] completed"

distclean: clean
	@echo "[$@]"
	@ $(RM) -rf $(TARGET_DIR)
	@echo "[$@] completed"

$(TARGET_DIR):
	@echo "[$@]"
	@mkdir -p $@
	@echo "[$@] completed"

poco:
	@echo "[$@]"
	@if ! [ -d $(TARGET_LIB_DIR)/libPocoFoundation.so ]; then \
		if ! [ -d ./.tmp ]; then \
		  mkdir .tmp;	\
		fi;	\
		tar xf $(libpoco_src_path) -C .tmp;	\
		cd .tmp/poco-$(poco_ver); \
		./configure; \
		make; \
		make install DESTDIR=$(TARGET_DIR);	\
		rm -rf .tmp;	\
	fi
	@echo "[$@] completed"

sdk: $(TARGET_DIR) poco
	@echo "[$@]"
	@tar xf $(librsi_dev_path) -C $(TARGET_DIR);
	@echo "[$@] completed"

configure:
	@echo "[$@]"
	@if [[  -z "${ARCH}" ]]; then       \
		make sdk;	\
	fi

	@if ! [ -d $(VENDOR_DIR)/$(BUILD_DIR) ]; then \
		mkdir $(VENDOR_DIR)/$(BUILD_DIR);	\
		echo ${CONFIG_ARGS} ;	\
		export RSI_ROOT_DIR=$(SYSROOT)/lge/app_ro/ ;	\
		cd $(VENDOR_DIR)/$(BUILD_DIR) && cmake ${CONFIG_ARGS} .. ;		\
	fi
	@echo "[$@] completed"

install: build
	@echo "[$@]"
	@if [[ ! -z "${TARGET_BOARD_IP}" ]]; then       \
		rsync -avzh $(TARGET_DIR)/usr/local/bin/rsiclient/* root@${TARGET_BOARD_IP}:/home/$(USER)/bin/; \
	else    \
		echo "You need set TARGET_BOARD_IP.";   \
	fi
	@echo "[$@] completed"


adb_push:
	@echo "[$@] "
	@adb push ${TARGET_DIR}/usr/local/ /usr/local/
	@echo "[$@] completed"
