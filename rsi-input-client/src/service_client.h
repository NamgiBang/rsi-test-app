/*
 * browser_service_client.h
 *
 *  Created on: May 28, 2018
 *      Author: buttonfly
 */

#ifndef ACCESS_SERVICE_CLIENT_H_
#define ACCESS_SERVICE_CLIENT_H_

#include <functional>
#include <unordered_map>
#include <mutex>
#include <condition_variable>
#include <chrono>
#include <thread>

#include "RsiClient.h"
#include "WsSubscribeClient.h"
#include "RsiClientException.h"
#include <functional>
#include <ostream>
#include <string>
#include <memory>

namespace a {

class ServiceClient {
public:
  enum HttpMethod{
    HTTP_METHOD_POST,
    HTTP_METHOD_PUT,
    HTTP_METHOD_GET,
    HTTP_METHOD_DELETE
  };

  ServiceClient();
  virtual ~ServiceClient();

  virtual int WillPrepare() { return 0;};
  virtual void Prepared(){};

  virtual int Prepare();

  void OnMessage(SAI::WsSubscribeClient &client, const SAI::WsClientMessage &payload);
  void OnSuccess(SAI::WsSubscribeClient &client, const SAI::WsClientMessage &payload);
  void OnFail(SAI::WsSubscribeClient &client, const SAI::WsClientMessage &payload);

  void SetEnableLog(bool enable) {log_enabled_ = enable;}

  std::unique_ptr<SAI::RsiServerResponse> SendHttpRequest(const std::string &url);
  std::unique_ptr<SAI::RsiServerResponse> SendHttpRequest(const std::string &url,
                                                                                                              const std::string &body,
                                                                                                              HttpMethod http_method);

  std::unique_ptr<SAI::RsiClientRequest> ResolveHttpMethod(const std::string &url,
                                                                                                              const std::string &body,
                                                                                                              HttpMethod http_method);


  static bool IsRestResponseStatusOk(const std::string &http_response);
  static bool IsHttpResponseStatusOk(int status_code);

  std::string *getFileList(){return FileList;}
  int getListNumber(){return ListNumber;}

protected:
  SAI::RsiClient::SharedPtr rsi_client_;
  SAI::WsSubscribeClient::SharedPtr ws_client_;

  std::mutex lck_;
  std::condition_variable cond_;
  std::string FileList[20];
  int ListNumber = 0;


//  std::string url_;
  uint16_t port_=0;

  bool log_enabled_=true;
};

} /* namespace a */

#endif /* ACCESS_BROWSER_SERVICE_CLIENT_H_ */
