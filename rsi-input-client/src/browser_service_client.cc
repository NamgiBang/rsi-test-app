/*
 * brwser_service_client.cc
 *
 *  Created on: May 28, 2018
 *      Author: buttonfly
 */

#include "browser_service_client.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cstring>
#include "RsiClientFactory.h"
#include "WsSubscribeClientFactory.h"
#include "Poco/Net/NetException.h"
#include "Poco/JSON/Parser.h"

#include "log_message.h"

namespace a {

const std::string BrowserServiceClient::kPostIdentifier="access";

const int BrowserServiceClient::kDefaultServicePort=9980;
const char BrowserServiceClient::kDefaultEnginesUrl[]="/browser/engines";
const char BrowserServiceClient::kDefaultInstancesUrl[]="/browser/instances";

BrowserServiceClient::BrowserServiceClient() {
}

BrowserServiceClient::~BrowserServiceClient() {
}

int BrowserServiceClient::WillPrepare() {
  if(port_==0) port_ = kDefaultServicePort;
  engines_url_= kDefaultEnginesUrl;
  instances_url_= kDefaultInstancesUrl;

  return 0;
}

void BrowserServiceClient::Prepared() {

  try {
    if( ! ws_client_->subscribe(instances_url_,
        new SAI::WsClientMessageCallback<ServiceClient>(this,
            &ServiceClient::OnMessage,
            &ServiceClient::OnSuccess,
            &ServiceClient::OnFail)) ) {
      LOG(ERROR) << __func__ <<  " failed to subscribe " << instances_url_;
      return;
    }
  }
  catch (SAI::RsiClientException &e) {
    LOG(ERROR) << __func__ << " RsiClientException : " << e.what();
    return;
  }
  catch (Poco::Net::ConnectionResetException &e) {
    LOG(ERROR) << __func__ << " ConnectionResetException : " << e.what();
    return;
  }
  catch (std::exception &e) {
    LOG(ERROR) << __func__ << " Unhandled Exception : " << e.what();
    return;
  }
}

} /* namespace a */
