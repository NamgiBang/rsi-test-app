
#include "browser_service_client.h"
#include <memory>
#include "log_message.h"
#include <string>
#include "Poco/Net/NetException.h"
#include "Poco/JSON/Parser.h"
#include "Poco/JSON/Query.h"
#include "Poco/JSON/Object.h"

int main(int argc, char *argv[]) {

  uint16_t port = 9980;
  if(argv[1]) {
    port = std::stoi(argv[1]);
  }

  std::unique_ptr<a::BrowserServiceClient> browser_service_client{new a::BrowserServiceClient()};
  browser_service_client->SetPort(port);
  browser_service_client->Prepare();

  LOG(INFO) << " subscribed!";
  getchar();


  {
    LOG(INFO) << " sending: to \/browser";
    auto response = browser_service_client->SendHttpRequest("/browser");
    if(response) {
      LOG(INFO) << " response: " << response->getBody();
    }
    else {
      LOG(INFO) << " no return";
    }
  }

  getchar();

  {
    LOG(INFO) << "[GET] sending: to \/browser/engines";
    auto response = browser_service_client->SendHttpRequest("/browser/engines");
    if(response) {
      LOG(INFO) << " response: " << response->getBody();


      //
      do {
        Poco::JSON::Parser parser;
        Poco::Dynamic::Var root = parser.parse(response->getBody());

        Poco::JSON::Query query(root);
        Poco::Dynamic::Var data = query.find("data");
        if(! data.isEmpty()) {
          Poco::JSON::Array::Ptr arr = data.extract<Poco::JSON::Array::Ptr>();
          Poco::Dynamic::Var var = arr->get(0);
          Poco::JSON::Object::Ptr object = var.extract<Poco::JSON::Object::Ptr>();
          if(! object->has("id")) break;


          std::string id = object->get("id");
          LOG(INFO) << " id: " << id;


          // 1. GET element
          do {
            LOG(INFO) << "[GET] sending: to   \/browser/engines/" << id;
            auto response = browser_service_client->SendHttpRequest("/browser/engines/" + id);
            if(response) {
              LOG(INFO) << " response: " << response->getBody();
            }
            else {
              LOG(INFO) << " no return";
            }
          }while(0);

          getchar();

          // 2. POST element
          do {
            LOG(INFO) << "[POST] sending: to   \/browser/engines/" << id;
            auto response = browser_service_client->SendHttpRequest("/browser/engines/" + id, "{\"acceptLanguages\":\"RU\",\"factoryReset\":true}",a::ServiceClient::HTTP_METHOD_POST);
            if(response) {
              LOG(INFO) << " response: " << response->getBody();
            }
            else {
              LOG(INFO) << " no return";
            }
          }while(0);

          getchar();
          // 3. Verify 1. GET element
          do {
            LOG(INFO) << "[GET] sending: to   \/browser/engines/" << id;
            auto response = browser_service_client->SendHttpRequest("/browser/engines/" + id);
            if(response) {
              LOG(INFO) << " response: " << response->getBody();
            }
            else {
              LOG(INFO) << " no return";
            }
          }while(0);
        }

        getchar();

      } while(0);

      //
    }
    else {
      LOG(INFO) << " no return";
    }
  }

  getchar();

//  {
//    LOG(INFO) << "[POST] sending: to \/browser/engines";
//    auto response = browser_service_client->SendHttpRequest("/browser/engines", "" , a::ServiceClient::HTTP_METHOD_POST);
//    if(response) {
//      LOG(INFO) << " response: " << response->getBody();
//    }
//    else {
//      LOG(INFO) << " no return";
//    }
//  }
//
//  getchar();


  return 0;
}
