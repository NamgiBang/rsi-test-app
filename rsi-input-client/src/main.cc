
#include "browser_service_client.h"
#include <memory>
#include "log_message.h"
#include <string>

int main(int argc, char *argv[]) {

  uint16_t port = 9980;
  if(argv[1]) {
    port = std::stoi(argv[1]);
  }

  std::unique_ptr<a::BrowserServiceClient> browser_service_client{new a::BrowserServiceClient()};
  browser_service_client->SetPort(port);
  browser_service_client->Prepare();

  LOG(INFO) << " subscribed!";
  getchar();


  {
    LOG(INFO) << " sending: to \/browser";
    auto response = browser_service_client->SendHttpRequest("/browser");
    if(response) {
      LOG(INFO) << " response: " << response->getBody();
    }
    else {
      LOG(INFO) << " no return";
    }
  }

  getchar();

  {
    LOG(INFO) << "[GET] sending: to \/browser/engines";
    auto response = browser_service_client->SendHttpRequest("/browser/engines");
    if(response) {
      LOG(INFO) << " response: " << response->getBody();
    }
    else {
      LOG(INFO) << " no return";
    }
  }

  getchar();

  {
    LOG(INFO) << "[POST] sending: to \/browser/engines";
    auto response = browser_service_client->SendHttpRequest("/browser/engines", "" , a::ServiceClient::HTTP_METHOD_POST);
    if(response) {
      LOG(INFO) << " response: " << response->getBody();
    }
    else {
      LOG(INFO) << " no return";
    }
  }

  getchar();

  {
    LOG(INFO) << "[GET] sending: to \/browser/instances";
    auto response = browser_service_client->SendHttpRequest("/browser/instances");
    if(response) {
      LOG(INFO) << " response: " << response->getBody();
    }
    else {
      LOG(INFO) << " no return";
    }
  }

  getchar();

  {
    LOG(INFO) << "[POST] sending: to \/browser/instances";
    auto response = browser_service_client->SendHttpRequest("/browser/instances", "", a::ServiceClient::HTTP_METHOD_POST);
    if(response) {
      LOG(INFO) << " response: " << response->getBody();
    }
    else {
      LOG(INFO) << " no return";
    }
  }

  getchar();

  return 0;
}
