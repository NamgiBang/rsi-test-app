/*
 * browser_service_client.h
 *
 *  Created on: May 28, 2018
 *      Author: buttonfly
 */

#ifndef ACCESS_BROWSER_SERVICE_CLIENT_H_
#define ACCESS_BROWSER_SERVICE_CLIENT_H_

#include <functional>
#include <unordered_map>
#include <mutex>
#include <condition_variable>
#include <chrono>
#include <thread>

#include "RsiClient.h"
#include "WsSubscribeClient.h"
#include "RsiClientException.h"
#include <functional>
#include <ostream>
#include <string>
#include <memory>

#include "service_client.h"

namespace a {

class BrowserServiceClient : public ServiceClient{
public:
  BrowserServiceClient();
  virtual ~BrowserServiceClient();

  void SetPort(uint16_t port) { port_=port; }
  void SetUrl(std::string &url) { instances_url_=url; }
  int WillPrepare() override;
  void Prepared() override;

public:
  static const std::string kPostIdentifier;
  static const char kDefaultEnginesUrl[];
  static const char kDefaultInstancesUrl[];
  static const int kDefaultServicePort;

private:
  std::string engines_url_;
  std::string instances_url_;
};

} /* namespace a */

#endif /* ACCESS_BROWSER_SERVICE_CLIENT_H_ */
