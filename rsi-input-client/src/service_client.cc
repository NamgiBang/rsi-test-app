/*
 * brwser_service_client.cc
 *
 *  Created on: May 28, 2018
 *      Author: buttonfly
 */

#include "service_client.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cstring>
#include "RsiClientFactory.h"
#include "WsSubscribeClientFactory.h"
#include "Poco/Net/NetException.h"
#include "Poco/JSON/Parser.h"
#include "Poco/JSON/Query.h"
#include "Poco/JSON/Object.h"
#include "log_message.h"

namespace a {

ServiceClient::ServiceClient() {
}

ServiceClient::~ServiceClient() {
  std::unique_lock<std::mutex> lk(lck_);

  SAI::RsiClientConnectOption rsi_client_connect_option(port_);

  if(ws_client_) {
    ws_client_->unsubscribeAll();
  }

  SAI::WsSubscribeClientFactory::instance().remove(rsi_client_connect_option);
  SAI::RsiClientFactory::instance().remove(rsi_client_connect_option);
}

int ServiceClient::Prepare() {

  int err;

  err = WillPrepare();
  if(err) {
    LOG(ERROR) << " failed to WillPrepare() ->" << err;
    return err;
  }

  try {
    rsi_client_ = SAI::RsiClientFactory::instance().get(port_);

    ws_client_= SAI::WsSubscribeClientFactory::instance().get(port_);

    if((!rsi_client_) || (!ws_client_)) {
      LOG(ERROR) << __func__ <<  " failed to construct RsiClient | WsSubscribeClient";
      return -1;
    }
  }
  catch (SAI::RsiClientException &e) {
    LOG(ERROR) << __func__ << " RsiClientException : " << e.what();
    return -1;
  }
  catch (Poco::Net::ConnectionResetException &e) {
    LOG(ERROR) << __func__ << " ConnectionResetException : " << e.what();
    return -1;
  }
  catch (std::exception &e) {
    LOG(ERROR) << __func__ << " Unhandled Exception : " << e.what();
    return -1;
  }

  return 0;
}


void ServiceClient::OnMessage(SAI::WsSubscribeClient &client, const SAI::WsClientMessage &payload) {
  //if(log_enabled_) LOG(INFO) <<  __func__ << " <<<< " << payload.getMessage();

  std::string FileName;

  Poco::JSON::Parser parser;
  Poco::Dynamic::Var root = parser.parse(payload.getMessage());
  Poco::JSON::Query query(root);
  Poco::Dynamic::Var data = query.find("data");
  Poco::JSON::Object::Ptr object = data.extract<Poco::JSON::Object::Ptr>();

  if(object->has("FileName")){
     FileName = object->get("FileName").toString();
     FileList[ListNumber] = FileName;
     if(ListNumber > 19){
       LOG(ERROR)<<"파일 리스트 초과!!";
       return;
     }
     if(FileName != ""){
       ListNumber++;
       for(int i = 0; i < ListNumber; i++){
         LOG(INFO)<<"FileList["<<i<<"] : "<<FileList[i];
       }
       LOG(INFO)<<"";
     }
  }


}

void ServiceClient::OnSuccess(SAI::WsSubscribeClient &client, const SAI::WsClientMessage &payload) {
  if(log_enabled_) LOG(INFO) <<  __func__ << " <<<< " << payload.getMessage();
}

void ServiceClient::OnFail(SAI::WsSubscribeClient &client, const SAI::WsClientMessage &payload) {
  if(log_enabled_) LOG(INFO) <<  __func__ << " <<<< " << payload.getMessage();
}

std::unique_ptr<SAI::RsiClientRequest> ServiceClient::ResolveHttpMethod(const std::string &url,
                                                                                                                      const std::string &body,
                                                                                                                      HttpMethod http_method) {
  std::unique_ptr<SAI::RsiClientRequest> request;

  switch(http_method) {
  case HTTP_METHOD_POST: {
    request.reset(new SAI::RsiClientPostRequest(url));
    if(! body.empty()) {
      request->setBody(body);
    }
    break;
  }
  case HTTP_METHOD_PUT: {
    request.reset(new SAI::RsiClientPutRequest(url));
    if(! body.empty()) {
      request->setBody(body);
    }
    break;
  }
  case HTTP_METHOD_DELETE: {
    request.reset(new SAI::RsiClientDeleteRequest(url));
    if(! body.empty()) {
      request->setBody(body);
    }
    break;
  }
  case HTTP_METHOD_GET: {
    request.reset(new SAI::RsiClientGetRequest(url));
    break;
  }
  default:
    if(log_enabled_)
      LOG(INFO) << __func__ << " Unhandled method: " << http_method;
    break;
  }

  return request;
}

std::unique_ptr<SAI::RsiServerResponse> ServiceClient::SendHttpRequest(const std::string &url) {
  return SendHttpRequest(url, "", HTTP_METHOD_GET);
}

std::unique_ptr<SAI::RsiServerResponse> ServiceClient::SendHttpRequest(const std::string &url,
                                                                                                                    const std::string &body,
                                                                                                                    HttpMethod http_method) {
  std::unique_lock<std::mutex> lk(lck_);

  if(! rsi_client_) {
    LOG(ERROR) << __func__ << " rsi_client_ is nil";
    return nullptr;
  }

  auto request = ResolveHttpMethod(url, body, http_method);
  if(! request) {
    LOG(ERROR) << __func__ << " failed to resolve a http-method";
    return nullptr;
  }

  auto response = std::unique_ptr<SAI::RsiServerResponse>{new SAI::RsiServerResponse(*request)};

  try {
    if(log_enabled_) LOG(INFO) <<   " >>>> " << request->getBody();

    rsi_client_->send(*(request.get()), *(response.get()));
    if(! IsHttpResponseStatusOk((int)response->getStatus())) {
      LOG(ERROR) << __func__ << " " << response->getBody();
      return nullptr;
    }

    if(! IsRestResponseStatusOk(response->getBody())) {
      LOG(ERROR) << __func__ << " " << response->getBody();
      return nullptr;
    }
  }
  catch (SAI::RsiClientException &e) {
    LOG(ERROR) << __func__ << " RsiClientException : " << e.what();
    return nullptr;
  }
  catch (Poco::Net::ConnectionResetException &e) {
    LOG(ERROR) << __func__ << " ConnectionResetException : " << e.what();
    return nullptr;
  }
  catch (std::exception &e) {
    LOG(ERROR) << __func__ << " Unhandled Exception : " << e.what();
    return nullptr;
  }
  return response;
}



bool ServiceClient::IsRestResponseStatusOk(const std::string &http_response) {
  LOG(INFO) <<  __func__;
  Poco::JSON::Parser parser;
  Poco::Dynamic::Var result = parser.parse(http_response);
  Poco::JSON::Object::Ptr object = result.extract<Poco::JSON::Object::Ptr>();
  if(! object->has("status")) {
    LOG(ERROR) << __func__ << " It must contain {status}";
    return false;
  }

  Poco::Dynamic::Var state = object->get("status");
  if(state.toString().compare("ok")==0) {
    return true;
  }
  return false;
}

bool ServiceClient::IsHttpResponseStatusOk(int status_code) {
  return (status_code >= 200) && (status_code < 300);
}



} /* namespace a */
