/*
 * rsi_client.h
 *
 *  Created on: May 28, 2018
 *      Author: buttonfly
 */

#ifndef RSI_CLIENT_H_
#define RSI_CLIENT_H_

#include <functional>
#include <unordered_map>
#include <mutex>
#include <condition_variable>
#include <chrono>
#include <thread>

#include "RsiClient.h"
#include "WsSubscribeClient.h"
#include <functional>
#include <ostream>
#include <string>
#include <memory>

class RsiMessageHandler {
public:
  RsiMessageHandler();
  virtual ~RsiMessageHandler();
  virtual void OnSubscribedData(const SAI::WsClientMessage &payload);
  virtual void OnSubscribSuccess(const SAI::WsClientMessage &payload);
  virtual void OnSubscribFail(const SAI::WsClientMessage &payload);
};

class RsiClient {
public:
  enum HttpMethod{
    HTTP_METHOD_POST,
    HTTP_METHOD_PUT,
    HTTP_METHOD_GET,
    HTTP_METHOD_DELETE
  };

  RsiClient();
  virtual ~RsiClient();

  void SetPort(uint16_t port) { port_= port; }
  uint16_t GetPort() { return port_; }

  // Takes ownership of |handler|, which will be destroyed when the RsiClient is.
  void SetMessageHandler(RsiMessageHandler* handler){ message_handler_.reset(handler); }

  virtual int WillPrepare() { return 0; }
  virtual void Prepared(){};

  virtual int Prepare();

  void Subscribe(const std::string &uri);
  void UnsubscribeAll();

  void SubscribDataCallback(SAI::WsSubscribeClient &client, const SAI::WsClientMessage &payload);
  void SubscribSuccessCallback(SAI::WsSubscribeClient &client, const SAI::WsClientMessage &payload);
  void SubscribFailCallback(SAI::WsSubscribeClient &client, const SAI::WsClientMessage &payload);

  void SetEnableLog(bool enable) {log_enabled_ = enable;}

  std::unique_ptr<SAI::RsiServerResponse> SendHttpRequest(const std::string &url);
  std::unique_ptr<SAI::RsiServerResponse> SendHttpRequest(const std::string &url,
                                                          const std::string &body,
                                                          HttpMethod http_method);

  std::unique_ptr<SAI::RsiClientRequest> ResolveHttpMethod(const std::string &url,
                                                          const std::string &body,
                                                          HttpMethod http_method);

  static bool IsRestResponseStatusOk(const std::string &http_response);
  static bool IsHttpResponseStatusOk(int status_code);


protected:
  SAI::RsiClient::SharedPtr rsi_client_;
  SAI::WsSubscribeClient::SharedPtr ws_client_;

  std::mutex lck_;
  std::condition_variable cond_;

  std::unique_ptr<RsiMessageHandler> message_handler_;

//  std::string url_;
  uint16_t port_=0;

  bool log_enabled_=true;
};


#endif /* RSI_CLIENT_H_ */
