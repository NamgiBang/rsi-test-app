#!/bin/sh

export BUILD_ROOT=$PWD
TOOLCHAIN_VER="3.0.0+snapshot"
TOOLCHAIN_PATH=/opt/poky-agl/$TOOLCHAIN_VER
TOOLCHAIN_ENV=environment-setup-aarch64-agl-linux
TOOLCHAIN_ENV_PATH=$TOOLCHAIN_PATH/$TOOLCHAIN_ENV

function install_package {
        dpkg -s $1 2>/dev/null >/dev/null || sudo apt-get -y install $1
}

install_package cmake

source $TOOLCHAIN_ENV_PATH

