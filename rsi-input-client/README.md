### How to build
```
source env-aarch64.sh
make
```

### How to install
```
export TARGET_BOARD_IP=${YOUR_TARGET_BOARD_IP}
(e.g. export TARGET_BOARD_IP=192.168.11.133)

make install

or 

make adb_push
```



### How to run
```
./rsiclient
```


### How to run input-client
```
./prerequisite-input-client
; run prerequisite-input-client once if input-service is newly executed.

./input-client
```
