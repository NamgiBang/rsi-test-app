
#ifndef CEF_INCLUDE_CEF_BASE_FAKE_H_
#define CEF_INCLUDE_CEF_BASE_FAKE_H_
#pragma once

#include "include/base/cef_basictypes.h"

///
// Template class that provides common functionality for CEF structure wrapping.
///
template <class traits>
class CefStructBase : public traits::struct_type {
 public:
  typedef typename traits::struct_type struct_type;

  CefStructBase() : attached_to_(NULL) {
    Init();
  }
  virtual ~CefStructBase() {
    // Only clear this object's data if it isn't currently attached to a
    // structure.
    if (!attached_to_)
      Clear(this);
  }

  CefStructBase(const CefStructBase& r) {
    Init();
    *this = r;
  }
  CefStructBase(const struct_type& r) {  // NOLINT(runtime/explicit)
    Init();
    *this = r;
  }

    ///
    // Clear this object's values.
    ///
    void Reset() {
      Clear(this);
      Init();
    }

    ///
    // Attach to the source structure's existing values. DetachTo() can be called
    // to insert the values back into the existing structure.
    ///
    void AttachTo(struct_type& source) {
      // Only clear this object's data if it isn't currently attached to a
      // structure.
      if (!attached_to_)
        Clear(this);

      // This object is now attached to the new structure.
      attached_to_ = &source;

      // Transfer ownership of the values from the source structure.
      memcpy(static_cast<struct_type*>(this), &source, sizeof(struct_type));
    }

    ///
    // Relinquish ownership of values to the target structure.
    ///
    void DetachTo(struct_type& target) {
      if (attached_to_ != &target) {
        // Clear the target structure's values only if we are not currently
        // attached to that structure.
        Clear(&target);
      }

      // Transfer ownership of the values to the target structure.
      memcpy(&target, static_cast<struct_type*>(this), sizeof(struct_type));

      // Remove the references from this object.
      Init();
    }

    ///
    // Set this object's values. If |copy| is true the source structure's values
    // will be copied instead of referenced.
    ///
    void Set(const struct_type& source, bool copy) {
      traits::set(&source, this, copy);
    }

    CefStructBase& operator=(const CefStructBase& s) {
      return operator=(static_cast<const struct_type&>(s));
    }

    CefStructBase& operator=(const struct_type& s) {
      Set(s, true);
      return *this;
    }

   protected:
    void Init() {
      memset(static_cast<struct_type*>(this), 0, sizeof(struct_type));
      attached_to_ = NULL;
      traits::init(this);
    }

    static void Clear(struct_type* s) { traits::clear(s); }

    struct_type* attached_to_;
  };

///
// Structure representing a touch point which corresponds to the DOM Touch
// interface.
///
typedef struct _cef_touch_point_t {
  ///
  // See the description of "identifier" in W3C Touch Events Level 2.
  // https://w3c.github.io/touch-events/#touch-interface
  ///
  int id;

  ///
  // X coordinate relative to the left side of the view.
  ///
  float x;

  ///
  // Y coordinate relative to the top side of the view.
  ///
  float y;

  ///
  // See the description of "radiusX" in W3C Touch Events Level 2.
  // https://w3c.github.io/touch-events/#touch-interface
  ///
  float radius_x;

  ///
  // See the description of "radiusY" in W3C Touch Events Level 2.
  // https://w3c.github.io/touch-events/#touch-interface
  ///
  float radius_y;

  ///
  // See the description of "rotationAngle" in W3C Touch Events Level 2.
  // https://w3c.github.io/touch-events/#touch-interface
  ///
  float rotation_angle;

  ///
  // See the description of "force" in W3C Touch Events Level 2.
  // https://w3c.github.io/touch-events/#touch-interface
  ///
  float force;

  ///
  // True (1) when the touch point is updated.
  ///
  int updated;
} cef_touch_point_t;

#define CEF_TOUCH_EVENT_MAX_TOUCH_POINTS 16

///
// Structure representing a touch event which corresponds to the DOM TouchEvent
// interface.
///
typedef struct _cef_touch_event_t {
  ///
  // A unique identifier for the touch event.
  ///
  uint32 id;

  ///
  // The number of touch points stored in |touches|.
  ///
  size_t touches_length;

  ///
  // An array of touch points.
  ///
  cef_touch_point_t touches[CEF_TOUCH_EVENT_MAX_TOUCH_POINTS];

  ///
  // Bit flags describing any pressed modifier keys. See
  // cef_event_flags_t for values.
  ///
  uint32 modifiers;
} cef_touch_event_t;



struct CefTouchEventTraits {
  typedef cef_touch_event_t struct_type;

  static inline void init(struct_type* s) {
    s->touches_length = 0;
  }

  static inline void clear(struct_type* s) {
    s->touches_length = 0;
  }

  static inline void set(const struct_type* src, struct_type* target,
      bool copy) {
    target->id = src->id;
    target->touches_length = src->touches_length;
    for (size_t i = 0; i < src->touches_length; ++i) {
      target->touches[i].id = src->touches[i].id;
      target->touches[i].x = src->touches[i].x;
      target->touches[i].y = src->touches[i].y;
      target->touches[i].radius_x = src->touches[i].radius_x;
      target->touches[i].radius_y = src->touches[i].radius_y;
      target->touches[i].rotation_angle = src->touches[i].rotation_angle;
      target->touches[i].force = src->touches[i].force;
      target->touches[i].updated = src->touches[i].updated;
    }
    target->modifiers = src->modifiers;
  }
};

///
// Class representing a touch event.
///
typedef CefStructBase<CefTouchEventTraits> CefTouchEvent;

#endif  // CEF_INCLUDE_CEF_BASE_FAKE_H_
