// Copyright(c) 2018 ACCESS CO., LTD. All rights reserved.
// Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.

#ifndef RSI_TOUCH_DELEGATE_IMPL_H_
#define RSI_TOUCH_DELEGATE_IMPL_H_

#include "rsi_client.h"
#include "rsi_touch_delegate.h"

namespace input {

class RsiTouchDelegateImpl : public RsiTouchDelegate,
                             public RsiMessageHandler
 {
public:
  RsiTouchDelegateImpl();
  virtual ~RsiTouchDelegateImpl();

  void OnSubscribedData(const SAI::WsClientMessage &payload) override;
  void OnSubscribSuccess(const SAI::WsClientMessage &payload) override;
  void OnSubscribFail(const SAI::WsClientMessage &payload) override;
  void ParseData(const std::string &payload_string) override;
  void ProcessData(const CefTouchEvent& event) override;
};

} /* namespace input */

#endif /* RSI_TOUCH_DELEGATE_IMPL_H_ */
