
#include "rsi_touch_input.h"
#include <string>
#include "log_message.h"
#include "rsi_touch_delegate_Impl.h"
#include "browser_service_client.h"
#include <dirent.h>

using namespace input;

void get_files_inDirectory(std::string capcher_file_list[], int *capcher_file_number){
  DIR *dir;
  struct dirent *ent;
  int i = 0;
  char* src = "./inputdump1";
  if ((dir = opendir (src)) != NULL) { /* 디렉토리를 열 수 있는 경우 */
    /* 디렉토리 안에 있는 모든 파일&디렉토리 출력 */
    while ((ent = readdir (dir)) != NULL) {
      if(std::string(ent->d_name) != "." && std::string(ent->d_name) != ".."){
        capcher_file_list[i] =  ent->d_name;
        i++;
        LOG(INFO)<<i<<"번째 동작 : "<< ent->d_name;

      }
    }
    *capcher_file_number = i;
    closedir (dir);
  } else { /* 디렉토리를 열 수 없는 경우 */
    perror ("fail open directory!!");
  }

}

int main(int argc, char *argv[]) {

  std::string a = "";
  std::string *FileList;
  int ListNumber = 1;
  int ExcuteNumber = -1;
  std::string capcher_file_list[20];
  int capcher_file_number = 0;
  int check_capcher = 0;
  uint16_t port = 9980;
  if(argv[1]) {
    port = std::stoi(argv[1]);
  }

  std::string uri ="/input/touchPanelStates/8ecbfaa9-778f-4e29-ba18-bc1c3d5a765f";

  std::unique_ptr<a::BrowserServiceClient> browser_service_client{new a::BrowserServiceClient()};
  browser_service_client->SetPort(port);
  browser_service_client->Prepare();
  browser_service_client->SetUrl(uri);

  LOG(INFO)<<"1번을 입력하면 Non-캡쳐 모드로 진행합니다.";
  LOG(INFO)<<"2번을 입력하면 캡쳐 모드로 진행합니다.";

  std::cin>>check_capcher;

  if (std::cin.fail()){
    LOG(INFO)<<"숫자로 입력하셔야 합니다.";
    LOG(INFO)<<"enter를 입력하면 프로그램을 종료합니다.";
    std::cin.clear();
    std::cin.ignore(256, '\n');
    getchar();
  }

  else if(check_capcher == 2){

    getchar();
    browser_service_client->Prepared();

    while(a != "y"){

      LOG(INFO)<<"캡쳐모드를 진행 중입니다.";
      LOG(INFO)<<"File list update waiting...";
      getchar();

      LOG(INFO)<<"캡쳐 모드를 끝내시겠습니까?";
      LOG(INFO)<<"y를 누르면 종료합니다.";
      LOG(INFO)<<"s를 누르면 계속 진행합니다.";
      std::cin>>a;

      if(a != "y"){
        LOG(INFO)<<"캡쳐 모드를 계속 진행합니다.";
      }
      else{
        ListNumber = browser_service_client->getListNumber();
        FileList = browser_service_client->getFileList();
      }
    }

    a = "";

    LOG(INFO) << "enter를 누르면 non-캡쳐 모드를 진행합니다.";
    getchar();

    while(a != "y"){
      for(int i = 0; i < ListNumber; i++){
        LOG(INFO)<<"FileList["<<i<<"] : "<< *(FileList + i);
      }

      LOG(INFO)<<"몇번 동작을 실행하시겠습니까?";
      std::cin>>ExcuteNumber;

      if (std::cin.fail()){
        LOG(INFO)<<"존재하지 않는 파일 번호입니다.";
        LOG(INFO)<<"다시 진행하려면 enter를 누르세요.";
        std::cin.clear();
        std::cin.ignore(256, '\n');
        getchar();
        ExcuteNumber = -1;
      }

      else if(ExcuteNumber < 0 || ExcuteNumber > ListNumber){
        LOG(INFO)<<"존재하지 않는 파일 번호입니다.";
        LOG(INFO)<<"다시 진행하려면 enter를 누르세요.";
        getchar();
      }

      else{
        auto response = browser_service_client->SendHttpRequest(uri, "{\"FileToBrowser\":\"" + *(FileList + ExcuteNumber) + "\"}",a::ServiceClient::HTTP_METHOD_POST);
        if(response) {
          LOG(INFO) << " response: " << response->getBody();
        }
        else {
          LOG(INFO) << " no return";
        }

        LOG(INFO)<<"non-캡쳐 모드를 끝내시겠습니까?";
        LOG(INFO)<<"y를 누르면 종료합니다.";
        LOG(INFO)<<"s를 누르면 계속 진행합니다.";

        std::cin>>a;
      }

    }
  }

  else if(check_capcher == 1){
    while(a != "y"){
      get_files_inDirectory(capcher_file_list, &capcher_file_number);
      LOG(INFO)<<"capcher_file_number : "<<capcher_file_number;

      LOG(INFO)<<"몇번 동작을 실행하시겠습니까?";
      std::cin>>ExcuteNumber;
      ExcuteNumber--;

      if (std::cin.fail()){
        LOG(INFO)<<"존재하지 않는 파일 번호입니다.";
        LOG(INFO)<<"다시 진행하려면 enter를 누르세요.";
        std::cin.clear();
        std::cin.ignore(256, '\n');
        getchar();
        ExcuteNumber = -1;
      }

      else if(ExcuteNumber < 0 || ExcuteNumber >= capcher_file_number){
        LOG(INFO)<<"존재하지 않는 파일 번호입니다.";
        LOG(INFO)<<"다시 진행하려면 enter를 누르세요.";
        getchar();
      }

      else{
        LOG(INFO)<< capcher_file_list[ExcuteNumber];
        auto response = browser_service_client->SendHttpRequest(uri, "{\"CapcherFileToBrowser\":\"" +  capcher_file_list[ExcuteNumber] + "\"}",a::ServiceClient::HTTP_METHOD_POST);
        if(response) {
          LOG(INFO) << " response: " << response->getBody();
        }
        else {
          LOG(INFO) << " no return";
        }

        LOG(INFO)<<"테스트를 끝내시겠습니까?";
        LOG(INFO)<<"y를 누르면 종료합니다.";
        LOG(INFO)<<"s를 누르면 계속 진행합니다.";

        std::cin>>a;
      }

    }
  }

  else{
    LOG(INFO)<<"1 또는 2를 입력하셔야 합니다.";
    LOG(INFO)<<"enter를 입력하면 프로그램을 종료합니다.";
    getchar();
  }

  return 0;
}
