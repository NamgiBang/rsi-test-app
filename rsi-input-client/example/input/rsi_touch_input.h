// Copyright(c) 2018 ACCESS CO., LTD. All rights reserved.
// Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.

#ifndef RSI_TOUCH_INPUT_H_
#define RSI_TOUCH_INPUT_H_

#include "rsi_client.h"

namespace input {

class RsiTouchInput : public RsiClient {
public:
  RsiTouchInput();
  virtual ~RsiTouchInput();

  void Initialize();
  void Subscribe();
  void Reset();

  std::unique_ptr<SAI::RsiServerResponse> GetInputTargets(std::string uuid = "");
  std::unique_ptr<SAI::RsiServerResponse> GetTouchPanelStates(std::string uuid = "");

protected:
  int WillPrepare() override;
  void Prepared() override;
};

} /* namespace input */

#endif /* RSI_TOUCH_INPUT_H_ */
