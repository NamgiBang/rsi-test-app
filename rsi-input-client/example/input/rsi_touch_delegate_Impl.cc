// Copyright(c) 2018 ACCESS CO., LTD. All rights reserved.
// Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.

#include "rsi_touch_delegate_Impl.h"
#include "Poco/Net/NetException.h"
#include "Poco/JSON/Parser.h"
#include "Poco/JSON/Query.h"
#include "Poco/JSON/Object.h"
#include "log_message.h"

using namespace input;

RsiTouchDelegateImpl::RsiTouchDelegateImpl() {
  LOG(INFO) <<  __func__ ;
}

RsiTouchDelegateImpl::~RsiTouchDelegateImpl() {
  LOG(INFO) <<  __func__ ;
}

void RsiTouchDelegateImpl::OnSubscribedData(const SAI::WsClientMessage &payload){
  LOG(INFO) <<  __func__ ;

  ParseData(payload.getMessage());
}

void RsiTouchDelegateImpl::OnSubscribSuccess(const SAI::WsClientMessage &payload){
  LOG(INFO) <<  __func__ ;
}

void RsiTouchDelegateImpl::OnSubscribFail(const SAI::WsClientMessage &payload){
  LOG(INFO) <<  __func__ ;
}

void RsiTouchDelegateImpl::ParseData(const std::string &payload_string) {
  LOG(INFO) <<  __func__ ;

  CefTouchEvent event;
  event.touches_length = 0; //totalTouchCount
  event.touches[0].id = 0; //id
  event.touches[0].x = 0; //touchesClientX
  event.touches[0].y = 0; // touchesClientY
  event.touches[0].radius_x = 0;
  event.touches[0].radius_y = 0;
  event.touches[0].rotation_angle = 0;
  event.touches[0].force = 0; //touchForces
  event.touches[0].updated = 0;

  Poco::JSON::Parser parser;
  Poco::Dynamic::Var result = parser.parse(payload_string);
  Poco::JSON::Object::Ptr objectptr = result.extract<Poco::JSON::Object::Ptr>();

  if(!objectptr->isArray("data") && !objectptr->isNull("data")){ //in case of Element

    Poco::Dynamic::Var object_data = objectptr->get("data");
    Poco::Dynamic::Var object_status = objectptr->get("status");
    Poco::Dynamic::Var object_timestamp = objectptr->get("timestamp");
    Poco::Dynamic::Var object_event = objectptr->get("event");
    Poco::Dynamic::Var object_type = objectptr->get("type");

    Poco::JSON::Object::Ptr subObjectptr = object_data.extract<Poco::JSON::Object::Ptr>();

    Poco::Dynamic::Var subObject_deltaTime = subObjectptr->get("deltaTime");
    Poco::JSON::Array::Ptr hoveringDistances_Array = subObjectptr->getArray("hoveringDistances");
    Poco::Dynamic::Var subObject_id = subObjectptr->get("id");
    Poco::Dynamic::Var subObject_totalTouchCount = subObjectptr->get("totalTouchCount");
    Poco::JSON::Array::Ptr touchActions_Array = subObjectptr->getArray("touchActions");
    Poco::JSON::Array::Ptr touchForces_Array = subObjectptr->getArray("touchForces");
    Poco::JSON::Array::Ptr touchIds_Array = subObjectptr->getArray("touchIds");
    Poco::JSON::Array::Ptr touchesClientX_Array = subObjectptr->getArray("touchesClientX");
    Poco::JSON::Array::Ptr touchesClientY_Array = subObjectptr->getArray("touchesClientY");
    Poco::JSON::Array::Ptr touchesScreenX_Array = subObjectptr->getArray("touchesScreenX");
    Poco::JSON::Array::Ptr touchesScreenY_Array = subObjectptr->getArray("touchesScreenY");
    Poco::Dynamic::Var subObject_uri = subObjectptr->get("uri");

    //make temporal variable for saving parsed data
    int deltaTime = 0;
    int *hoveringDistances = new int[hoveringDistances_Array->size()];
    int totalTouchCount = 0;
    std::string id = "";
    std::string *touchActions = new std::string[touchActions_Array->size()];
    int *touchForces = new int[touchForces_Array->size()];
    int *touchIds = new int[touchIds_Array->size()];
    int *touchesClientX = new int[touchesClientX_Array->size()];
    int *touchesClientY = new int[touchesClientY_Array->size()];
    int *touchesScreenX = new int[touchesScreenX_Array->size()];
    int *touchesScreenY = new int[touchesScreenY_Array->size()];
    std::string uri = "";
    bool status = false;
    int timestamp = 0;
    std::string event_data = "";
    std::string type = "";

    deltaTime = subObject_deltaTime;

    if(subObjectptr->has("hoveringDistances") && hoveringDistances_Array->size()){
      for(int i = 0; i < hoveringDistances_Array->size(); i++){
        hoveringDistances[i] = hoveringDistances_Array->get(i);
      }
    }

    if(subObjectptr->has("id")){
      id = subObject_id.toString();
    }

    if(subObjectptr->has("totalTouchCount")){
      totalTouchCount = subObject_totalTouchCount;
    }

    if(subObjectptr->has("touchActions") && touchActions_Array->size()){
      for(int i = 0; i < touchActions_Array->size(); i++){
        touchActions[i] = touchActions_Array->get(i).toString();
      }
    }

    if(subObjectptr->has("touchForces") && touchForces_Array->size()){
      for(int i = 0; i < touchForces_Array->size(); i++){
        touchForces[i] = touchForces_Array->get(i);
      }
    }

    if(subObjectptr->has("touchIds") && touchIds_Array->size()){
      for(int i = 0; i < touchIds_Array->size(); i++){
        touchIds[i] = touchIds_Array->get(i);
      }
    }

    if(subObjectptr->has("touchesClientX") && touchesClientX_Array->size()){
      for(int i = 0; i < touchesClientX_Array->size(); i++){
        touchesClientX[i] = touchesClientX_Array->get(i);
      }
    }

    if(subObjectptr->has("touchesClientY") && touchesClientY_Array->size()){
      for(int i = 0; i < touchesClientY_Array->size(); i++){
        touchesClientY[i] = touchesClientY_Array->get(i);
      }
    }

    if(subObjectptr->has("touchesScreenX") && touchesScreenX_Array->size()){
      for(int i = 0; i < touchesScreenX_Array->size(); i++){
        touchesScreenX[i] = touchesScreenX_Array->get(i);
      }
    }

    if(subObjectptr->has("touchesScreenY") && touchesScreenY_Array->size()){
      for(int i = 0; i < touchesScreenY_Array->size(); i++){
        touchesScreenY[i] = touchesScreenY_Array->get(i);
      }
    }

    if(subObjectptr->has("uri")){
      uri = subObject_uri.toString();
    }

    if(objectptr->has("status")){
      if(object_status.toString() == "ok"){
        status = true;
      }
    }

    if(objectptr->has("event")){
        event_data = object_event.toString();
    }

    if(objectptr->has("type")){
        type = object_type.toString();
    }

    timestamp = object_timestamp;

    event.touches_length = touchesClientX_Array->size();
    for(int i = 0; i < event.touches_length; i++){ //insert to cef instance
      event.touches[i].x = touchesClientX[i];
      event.touches[i].y = touchesClientY[i];
    }

    delete [] hoveringDistances;
    delete [] touchActions;
    delete [] touchForces;
    delete [] touchIds;
    delete [] touchesClientX;
    delete [] touchesClientY;
    delete [] touchesScreenX;
    delete [] touchesScreenY;

    ProcessData(event);

  }//in case of Element

  else if(objectptr->has("data") || objectptr->has("service")){//in case of Resourse

    Poco::JSON::Array::Ptr Resource_data_Array = objectptr->getArray("data");
    Poco::Dynamic::Var object_service = objectptr->get("service");
    Poco::Dynamic::Var object_status = objectptr->get("status");
    Poco::Dynamic::Var object_timestamp = objectptr->get("timestamp");
    Poco::JSON::Object::Ptr subObject_service = object_service.extract<Poco::JSON::Object::Ptr>();
    Poco::Dynamic::Var service_description = subObject_service->get("description");
    Poco::Dynamic::Var service_id = subObject_service->get("id");
    Poco::Dynamic::Var service_name = subObject_service->get("name");
    Poco::Dynamic::Var service_port = subObject_service->get("port");
    Poco::JSON::Array::Ptr service_privileges = subObject_service->getArray("privileges");
    Poco::JSON::Array::Ptr service_serviceCategories = subObject_service->getArray("serviceCategories");
    Poco::Dynamic::Var service_uri = subObject_service->get("uri");
    Poco::Dynamic::Var service_versions = subObject_service->get("versions");

    std::string **Resource_data = new std::string*[Resource_data_Array->size()];
    for(int i = 0; i < 3; i++){
      Resource_data[i] = new std::string[3];
    }
    std::string description = "";
    std::string id ="";
    std::string name ="";
    int port = 0;
    std::string *privileges = new std::string[service_privileges->size()];
    std::string *serviceCategories = new std::string[service_serviceCategories->size()];
    std::string uri = "";
    std::string versions = "";
    bool status = false;
    int timestamp = 0;

    if(objectptr->has("data") && Resource_data_Array->size()){
      for(int i = 0; i < Resource_data_Array->size(); i++){
        Poco::Dynamic::Var Resource_data_Object = Resource_data_Array->get(i);
        Poco::JSON::Object::Ptr Resource_data_ObjectPtr = Resource_data_Object.extract<Poco::JSON::Object::Ptr>();
        if(Resource_data_ObjectPtr->has("id")){
          Poco::Dynamic::Var Resource_data_id = Resource_data_ObjectPtr->get("id");
          Resource_data[i][0] = Resource_data_id.toString();
        }
        if(Resource_data_ObjectPtr->has("name")){
          Poco::Dynamic::Var Resource_data_name = Resource_data_ObjectPtr->get("name");
          Resource_data[i][1] = Resource_data_name.toString();
        }
        if(Resource_data_ObjectPtr->has("uri")){
          Poco::Dynamic::Var Resource_data_uri = Resource_data_ObjectPtr->get("uri");
          Resource_data[i][2] = Resource_data_uri.toString();
        }
      }
    }

    if(subObject_service->has("description")){
      description = service_description.toString();
    }

    if(subObject_service->has("id")){
      id = service_id.toString();
    }

    if(subObject_service->has("name")){
      name = service_name.toString();
    }

    if(subObject_service->has("port")){
      port = service_port;
    }

    if(subObject_service->has("privileges") && service_privileges->size()){
      for(int i = 0; i < service_privileges->size(); i++){
        privileges[i] = service_privileges->get(i).toString();
      }
    }

    if(subObject_service->has("serviceCategories") && service_serviceCategories->size()){
      for(int i = 0; i < service_serviceCategories->size(); i++){
        serviceCategories[i] = service_serviceCategories->get(i).toString();
      }
    }

    if(subObject_service->has("uri")){
      uri = service_uri.toString();
    }

    if(subObject_service->has("versions")){
      versions = service_versions.toString();
    }

    if(objectptr->has("status")){
      if(object_status.toString() == "ok"){
        status = true;
      }
    }

    if(objectptr->has("timestamp")){
      timestamp = object_timestamp;
    }

    delete [] privileges;
    delete [] serviceCategories;
    if(Resource_data_Array->size()){
      for(int i = 0; i < 3; ++i){
        delete [] Resource_data[i];
      }
    }
    delete [] Resource_data;
  }//in case of Resourse

  else{
    LOG(INFO)<<"Nothing data or syntax error";
  }

}

void RsiTouchDelegateImpl::ProcessData(const CefTouchEvent& event) {
  LOG(INFO) <<  __func__ ;

  LOG(INFO) << "event.touches_length: "  << event.touches_length ;
  for (size_t i = 0; i < event.touches_length; ++i) {
    LOG(INFO) << "touch[" << i << "] x" << event.touches[i].x ;
    LOG(INFO) << "touch[" << i << "] y" << event.touches[i].y ;
  }
  // CefPostTask(TID_UI, base::Bind(&dispatch, handler, event_type, event));
}
