// Copyright(c) 2018 ACCESS CO., LTD. All rights reserved.
// Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.

#ifndef RSI_TOUCH_DELEGATE_H_
#define RSI_TOUCH_DELEGATE_H_

#include "include/internal/cef_types_wrappers_fake.h"

namespace input {

class RsiTouchDelegate {
public:
  virtual ~RsiTouchDelegate() {}
  virtual void ParseData(const std::string &payload_string) = 0;  
  virtual void ProcessData(const CefTouchEvent& event) = 0;
};

} /* namespace input */

#endif /* RSI_TOUCH_DELEGATE_H_ */
