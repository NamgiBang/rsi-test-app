
#include "rsi_client.h"
#include <memory>
#include "log_message.h"
#include <string>
#include "Poco/Net/NetException.h"
#include "Poco/JSON/Parser.h"
#include "Poco/JSON/Query.h"
#include "Poco/JSON/Object.h"


int main(int argc, char *argv[]) {

  uint16_t port = 9980;
  if(argv[1]) {
    port = std::stoi(argv[1]);
  }

  std::unique_ptr<RsiClient> rsi_touch_input_client{new RsiClient()};
  rsi_touch_input_client->SetPort(port);
  rsi_touch_input_client->Prepare();

  {
    LOG(INFO) << "[GET]  sending: to /input";
    auto response = rsi_touch_input_client->SendHttpRequest("/input");
    if(response) {
      LOG(INFO) << " response: " << response->getBody();
    }
    else {
      LOG(INFO) << " no return";
    }
  }


  LOG(INFO) << " Press 'enter' to perform a [GET] request to /input/touchPanelStates";
//  getchar();


  {
    LOG(INFO) << "[GET] sending: to /input/touchPanelStates";
    auto response = rsi_touch_input_client->SendHttpRequest("/input/touchPanelStates");
    if(response) {
      LOG(INFO) << " response: " << response->getBody();
    }
    else {
      LOG(INFO) << " no return";
    }
  }


  LOG(INFO) << " Press 'enter' to perform a [POST] request to /input/inputTargets";
//  getchar();

  {
    LOG(INFO) << "[POST] sending: to /input/inputTargets";
    auto response = rsi_touch_input_client->SendHttpRequest("/input/inputTargets", "{\"enum\":[\"touch\"],\"name\":\"access-input-sets\"}" , RsiClient::HTTP_METHOD_POST);
    if(response) {
      LOG(INFO) << " response: " << response->getBody();
    }
    else {
      LOG(INFO) << " no return";
    }
  }


  {
    LOG(INFO) << "[GET]  sending: to /input/inputTargets";
    auto response = rsi_touch_input_client->SendHttpRequest("/input/inputTargets");
    if(response) {
      LOG(INFO) << " response: " << response->getBody();
    }
    else {
      LOG(INFO) << " no return";
    }
  }

  return 0;
}
