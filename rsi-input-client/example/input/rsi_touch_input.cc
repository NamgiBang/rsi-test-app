// Copyright(c) 2018 ACCESS CO., LTD. All rights reserved.
// Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.

#include "rsi_touch_input.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cstring>
#include "rsi_touch_delegate_Impl.h"
#include "log_message.h"

const std::string kPostIdentifier="access";

const int kDefaultServicePort=9980;
const char kDefaultInputTargetsUrl[]="/input/inputTargets";
const char kDefaultInputTouchPanelStatesUrl[]="/input/touchPanelStates";

const std::string kAccessInputTarget = "2f1b2f50-3046-449c-a644-f25eeb7c62b5";
const std::string kAccessTouchPanelState = "8ecbfaa9-778f-4e29-ba18-bc1c3d5a765f";

using namespace input;

RsiTouchInput::RsiTouchInput() : RsiClient() {
}

RsiTouchInput::~RsiTouchInput() {
  UnsubscribeAll();
}

void RsiTouchInput::Initialize() {
  Prepare();
}

int RsiTouchInput::WillPrepare(){
  SetPort(kDefaultServicePort);
  SetMessageHandler(new RsiTouchDelegateImpl());
}

void RsiTouchInput::Prepared(){
  //Subscribe();
}

void RsiTouchInput::Subscribe() {
  std::string input_target_url = kDefaultInputTargetsUrl;

#if 0
  auto response = SendHttpRequest(input_target_url);
  if(response) {
    LOG(INFO) << " response: " << response->getBody();
  }
  else {
    LOG(INFO) << " no return";
  }
#endif

  std::string touch_panel_state_url = kDefaultInputTouchPanelStatesUrl;
  touch_panel_state_url += "/" + kAccessTouchPanelState;
  RsiClient::Subscribe(touch_panel_state_url);

  LOG(INFO) << " subscribed!";
}


void RsiTouchInput::Reset() {
  UnsubscribeAll();
}

std::unique_ptr<SAI::RsiServerResponse> RsiTouchInput::GetInputTargets(std::string uuid) {
  std::string detailedAddress;
  if (!uuid.empty())
    detailedAddress = "/" + uuid;
  LOG(INFO) << "[GET] sending: to /input/inputTargets" + detailedAddress;
  auto response = SendHttpRequest("/input/inputTargets" + detailedAddress);
  if(response) {
    LOG(INFO) << " response: " << response->getBody();
  }
  else {
    LOG(INFO) << " no return";
  }

  return response;
}


std::unique_ptr<SAI::RsiServerResponse> RsiTouchInput::GetTouchPanelStates(std::string uuid) {
  std::string detailedAddress;
  if (!uuid.empty())
    detailedAddress = "/" + uuid;

  LOG(INFO) << "[GET] sending: to /input/touchPanelStates" + detailedAddress;
  auto response = SendHttpRequest("/input/touchPanelStates" + detailedAddress);
  if(response) {
    LOG(INFO) << " response: " << response->getBody();
  }
  else {
    LOG(INFO) << " no return";
  }

  return response;
}
