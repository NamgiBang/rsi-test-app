# - Try to find Rsi
#
# The following variables are optionally searched for defaults
#  RSIC_ROOT_DIR:            Base directory where all RSI components are found
#
# The following are set after configuration is done:
#  RSIC_FOUND
#  RSIC_INCLUDE_DIRS
#  RSIC_LIBRARIES
#  RSIC_LIBRARYRARY_DIRS
include(FindPackageHandleStandardArgs)
set(RSIC_ROOT_DIR "$ENV{RSI_ROOT_DIR}" CACHE PATH "Folder contains Rsic rsic")

find_path(RSIC_INCLUDE_DIR librsic/RsiClient.h
    PATHS ${RSIC_ROOT_DIR})

find_library(RSIC_LIBRARY rsic
    PATHS ${RSIC_ROOT_DIR}
    PATH_SUFFIXES lib64)


find_package_handle_standard_args(Rsic DEFAULT_MSG RSIC_INCLUDE_DIR RSIC_LIBRARY)
if(RSIC_FOUND)
  set(RSIC_INCLUDE_DIRS ${RSIC_INCLUDE_DIR} ${RSIC_INCLUDE_DIR}/librsic)
  set(RSIC_LIBRARIES ${RSIC_LIBRARY})
  message(STATUS "Found rsic    (include: ${RSIC_INCLUDE_DIRS}, library: ${RSIC_LIBRARIES})")
  mark_as_advanced(RSIC_ROOT_DIR RSIC_LIBRARY_RELEASE RSIC_LIBRARY_DEBUG
                                 RSIC_LIBRARY RSI_INCLUDE_DIR)
endif()
