# - Try to find Rsi
#
# The following variables are optionally searched for defaults
#  RSI_ROOT_DIR:            Base directory where all RSI components are found
#
# The following are set after configuration is done:
#  RSI_FOUND
#  RSI_INCLUDE_DIRS
#  RSI_LIBRARIES
#  RSI_LIBRARYRARY_DIRS
include(FindPackageHandleStandardArgs)
set(RSI_ROOT_DIR "$ENV{RSI_ROOT_DIR}" CACHE PATH "Folder contains Rsi rsi")

find_path(RSI_INCLUDE_DIR librsi/RsiLib.h
    PATHS ${RSI_ROOT_DIR})

find_library(RSI_LIBRARY rsi
    PATHS ${RSI_ROOT_DIR}
    PATH_SUFFIXES lib lib64)

find_library(WOLFSSL_LIBRARY wolfssl
    PATHS ${RSI_ROOT_DIR}
    PATH_SUFFIXES lib lib64)

find_library(NETLINK_LIBRARY netlink
    PATHS ${RSI_ROOT_DIR}
    PATH_SUFFIXES lib lib64)

find_package_handle_standard_args(Rsi DEFAULT_MSG RSI_INCLUDE_DIR RSI_LIBRARY)
if(RSI_FOUND)
  set(RSI_INCLUDE_DIRS ${RSI_INCLUDE_DIR} ${RSI_INCLUDE_DIR}/librsi)
  set(RSI_LIBRARIES ${RSI_LIBRARY} ${WOLFSSL_LIBRARY} ${NETLINK_LIBRARY})
  message(STATUS "Found rsi    (include: ${RSI_INCLUDE_DIRS}, library: ${RSI_LIBRARIES})")
  mark_as_advanced(RSI_ROOT_DIR RSI_LIBRARY_RELEASE RSI_LIBRARY_DEBUG
                                 RSI_LIBRARY RSI_INCLUDE_DIR)
endif()
