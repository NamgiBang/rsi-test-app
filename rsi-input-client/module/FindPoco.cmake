# - Try to find Poco
#
# The following variables are optionally searched for defaults
#  POCO_ROOT_DIR:            Base directory where all POCO components are found
#
# The following are set after configuration is done:
#  POCO_FOUND
#  POCO_INCLUDE_DIRS
#  POCO_LIBRARIES
#  POCO_LIBRARYRARY_DIRS
include(FindPackageHandleStandardArgs)
set(POCO_ROOT_DIR "" CACHE PATH "Folder contains Poco poco")

find_path(POCO_INCLUDE_DIR Poco/Poco.h
    PATHS ${POCO_ROOT_DIR})

find_library(POCO_FOUNDATION_LIBRARY PocoFoundation
    PATHS ${POCO_ROOT_DIR}
    PATH_SUFFIXES lib lib64)

find_library(POCO_NET_LIBRARY PocoNet
    PATHS ${POCO_ROOT_DIR}
    PATH_SUFFIXES lib lib64)

find_library(POCO_JSON_LIBRARY PocoJSON
    PATHS ${POCO_ROOT_DIR}
    PATH_SUFFIXES lib lib64)

find_library(POCO_UTIL_LIBRARY PocoUtil
    PATHS ${POCO_ROOT_DIR}
    PATH_SUFFIXES lib lib64)

find_library(POCO_XML_LIBRARY PocoXML
    PATHS ${POCO_ROOT_DIR}
    PATH_SUFFIXES lib lib64)


find_package_handle_standard_args(Poco DEFAULT_MSG POCO_INCLUDE_DIR POCO_FOUNDATION_LIBRARY)
if(POCO_FOUND)
  set(POCO_INCLUDE_DIRS ${POCO_INCLUDE_DIR} ${POCO_INCLUDE_DIR}/Poco)
  set(POCO_LIBRARIES ${POCO_FOUNDATION_LIBRARY} ${POCO_NET_LIBRARY} ${POCO_JSON_LIBRARY} ${POCO_UTIL_LIBRARY} ${POCO_XML_LIBRARY})
  message(STATUS "Found poco    (include: ${POCO_INCLUDE_DIRS}, library: ${POCO_LIBRARIES})")
  mark_as_advanced(POCO_ROOT_DIR POCO_LIBRARY_RELEASE POCO_LIBRARY_DEBUG
                                 POCO_LIBRARY POCO_INCLUDE_DIR)
endif()
