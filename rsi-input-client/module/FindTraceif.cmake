# - Try to find Traceif
#
# The following variables are optionally searched for defaults
#  TRACEIF_ROOT_DIR:            Base directory where all TRACEIF components are found
#
# The following are set after configuration is done:
#  TRACEIF_FOUND
#  TRACEIF_INCLUDE_DIRS
#  TRACEIF_LIBRARIES
#  TRACEIF_LIBRARYRARY_DIRS
include(FindPackageHandleStandardArgs)
set(TRACEIF_ROOT_DIR "" CACHE PATH "Folder contains Traceif traceif")

find_path(TRACEIF_INCLUDE_DIR traceif.h
    PATHS ${TRACEIF_ROOT_DIR})

find_library(TRACEIF_LIBRARY traceif
    PATHS ${TRACEIF_ROOT_DIR}
    PATH_SUFFIXES lib lib64)


find_package_handle_standard_args(Traceif DEFAULT_MSG TRACEIF_INCLUDE_DIR TRACEIF_LIBRARY)
if(TRACEIF_FOUND)
  set(TRACEIF_INCLUDE_DIRS ${TRACEIF_INCLUDE_DIR})
  set(TRACEIF_LIBRARIES ${TRACEIF_LIBRARY})
  message(STATUS "Found traceif    (include: ${TRACEIF_INCLUDE_DIRS}, library: ${TRACEIF_LIBRARIES})")
  mark_as_advanced(TRACEIF_ROOT_DIR TRACEIF_LIBRARY_RELEASE TRACEIF_LIBRARY_DEBUG
                                 TRACEIF_LIBRARY TRACEIF_INCLUDE_DIR)
endif()
