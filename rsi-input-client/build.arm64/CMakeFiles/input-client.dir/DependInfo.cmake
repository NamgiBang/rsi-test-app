# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/namgibang/workplace/rsi-project/rsi-input-client/example/input/input_client.cc" "/home/namgibang/workplace/rsi-project/rsi-input-client/build.arm64/CMakeFiles/input-client.dir/example/input/input_client.cc.o"
  "/home/namgibang/workplace/rsi-project/rsi-input-client/example/input/rsi_touch_delegate_Impl.cc" "/home/namgibang/workplace/rsi-project/rsi-input-client/build.arm64/CMakeFiles/input-client.dir/example/input/rsi_touch_delegate_Impl.cc.o"
  "/home/namgibang/workplace/rsi-project/rsi-input-client/example/input/rsi_touch_input.cc" "/home/namgibang/workplace/rsi-project/rsi-input-client/build.arm64/CMakeFiles/input-client.dir/example/input/rsi_touch_input.cc.o"
  "/home/namgibang/workplace/rsi-project/rsi-input-client/src/browser_service_client.cc" "/home/namgibang/workplace/rsi-project/rsi-input-client/build.arm64/CMakeFiles/input-client.dir/src/browser_service_client.cc.o"
  "/home/namgibang/workplace/rsi-project/rsi-input-client/src/rsi_client.cc" "/home/namgibang/workplace/rsi-project/rsi-input-client/build.arm64/CMakeFiles/input-client.dir/src/rsi_client.cc.o"
  "/home/namgibang/workplace/rsi-project/rsi-input-client/src/service_client.cc" "/home/namgibang/workplace/rsi-project/rsi-input-client/build.arm64/CMakeFiles/input-client.dir/src/service_client.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "__STDC_CONSTANT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../"
  "../include"
  "../src"
  "../example"
  "../example/input"
  "/opt/poky-agl/3.0.0+snapshot/sysroots/aarch64-agl-linux/usr/include/librsi"
  "/opt/poky-agl/3.0.0+snapshot/sysroots/aarch64-agl-linux/usr/include/Poco"
  "/opt/poky-agl/3.0.0+snapshot/sysroots/aarch64-agl-linux/usr/include/librsic"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
